import discord
from datetime import datetime
from os import listdir, path
from sqlite3 import connect

date = datetime.now()

commands = discord.ext.commands



def get_prefix(bot: discord.ext.commands.Bot, message: discord.Message):
	"""Returns a server's prefix"""
	id = message.server.id

	connection = connect('serverdat.db')
	cursor = connection.cursor()

	cursor.execute('SELECT EXISTS(SELECT 1 FROM serverdat WHERE serverid=?', id)

	if cursor.fetchone():
		cursor.execute('SELECT serverprefix FROM serverdat WHERE serverid=?', id)

	return cursor.fetchone()



bot = commands.Bot(command_prefix=get_prefix())
client = discord.Client()



@bot.event
async def on_ready():
	print(f'Logged in as: {bot.user.name}, {bot.user.id}')
	print('-' * 64)
	print(f'Date: {date.month}/{date.day}/{date.year} at {date.hour}:{date.minute}:{date.second}')



@client.event()
async def on_server_join(server: discord.Server):
	connection = connect('serverdat.db')
	cursor = connection.cursor()

	serverdata = [server.id, '#']

	cursor.executemany('INSERT INTO serverdat VALUES (?,?)', serverdata)

	cursor.commit()
	cursor.close()



def load(extension_name: str, extension_dir: str = ''):
	"""Loads an extension."""

	try:
		bot.load_extension(extension_dir.replace('/', '.') + extension_dir and '.' or '' + extension_name)

	except (AttributeError, ImportError) as e:
		with f as open(f'logs/{date.month}-{date.day}-{date.year}-{date.hour}-{date.minute}-{date.second}', 'w+')
			f.write(e)

	print(f"{extension_dir.replace('/', '.') + extension_dir and '.' or '' + extension_name} loaded successfully.")



def unload(extension_name: str, extension_dir: str = ''):
	"""Unloads an extension."""

	bot.unload_extension(extension_dir.replace('/', '.') + extension_dir and '.' or '' + extension_name)
	print(f"{extension_dir.replace('/', '.') + extension_dir and '.' or '' + extension_name} unloaded successfully.")



@bot.command()
async def help(ctx):
	"""Sends a help screen to a user, defining every command."""

	help_text = ''

	for cog in startup_extensions:
		help_text += f'**{cog}**\n\n\n'
		cog_commands = get_cog_commands(cog)

		for command in cog_commands:
			help_text += f'**{command}:**\n'
			help_text += command.help + '\n\n'

		help_text += ('-' * 64) + '\n'

	send_overflow(ctx, help_text, ctx.message.author)

	

async def send_overflow(content: str, destination: discord.Channel):

	text_chunks = []

	if len(content) > 1900:
		amount_over = ceil(len(content) / 1900)

		# Iterating through every 1900 characters, then appending them in a splitlines()-esque manner.
		for i in range(1, amount_over+1):
			text_chunks.append(content[(i - 1) * 1900 : i * 1900 < len(content) and i * 1900 or -1])

		for chunk in help_text_chunks:
			await client.send_message(destination, chunk)

	else:
		await client.send_message(destination, content)

'''------------------------------------------------------------------------------------------------------------------------------------------------------------------------------'''

if __name__ == "__main__":

	first_time_setup_check = listdir('./')
	if 'serverdat.db' not in first_time_setup_check and 'userdat.db' not in first_time_setup_check:

		while True:
			do_setup = input('Databases not found. Begin first time setup? Y/N\n')

			if do_setup.upper() == 'Y':

				with f as open('serverdat.db', 'w+'):
					print('Creating database file serverdat...')
				with f as open('userdat.db', 'w+'):
					print('Creating database file userdat...')

				print('Creating connection to database file serverdat...')
				connection = connect('serverdat.db')
				cursor = connection.cursor()

				print('Creating table serverdat in database file serverdat...')
				cursor.execute('''CREATE TABLE serverdat
								(serverid real, serverprefix text)''')

				cursor.commit()
				cursor.close()

				print('Creating connection to database file userdat...')
				connection = connect('userdat.db')
				cursor = connection.cursor()

				print('Creating table userdat in database file userdat...')
				cursor.execute('''CREATE TABLE userdat
								(userID real, infractions real, ruleseverity real, mentalseverity real,
								sessionID text, helptype text, issues text, helpdescription text, helperdescription, text)''')

				cursor.commit()
				cursor.close()

				print('First-time setup is now complete.')

			elif do_setup.upper() == 'N':
				break

	startup_path = path.join('cogs')
	extensions = listdir(startup_path)

	# Parenthesis added purely for readability.
	# Checks to make sure it doesn't load __pycache__ and __init__.py in the case I decide to make it a package.
	extension_check = lambda ext: ('__' not in ext) and (ext.replace('.py', '')) or None
	cogs = [extension_check(extension) for extension in extensions]

    for extension in cogs:

        try:
            bot.load_extension(extension)

        except ImportError as e:
            exc = f'{type(e).__name__}: {e}'
            print(f'Failed to load extension {extension}\n{exc}')

    bot.run(token.txt)
    client.run(token.txt)