from ..lib import coroutines, users
from ..main import openhelp, openhelpers, client, send_overflow
from sqlite3 import connect
from sys import path

path.append('../')

class Connection:
	def __init__(self, bot: commands.Bot):
		self.bot = bot



	@commands.group(name='connect')
	async def start_connection(ctx):
		ctx.send('You are connected. Did you mean to envoke a subcommand?\nThe commands are: \'help\' for looking for help and \'helper\' for looking to help.')



	@make_connection.add_command(name='help')
	async def looking_for_help(ctx):
		values = {}
		helpers = {}

		user = users.load_user(ctx.message.author.id)
		issues = user.issues
		openhelp.append(user)

		for person in openhelpers:
			help_value = 0

			for helptype in person.helptype:
				if helptype in issues:
					help_value += 1

			if help_value not in values.keys():
				values[help_value] = person.userID

			else:
				values[help_value].append(person.userID)

			helpers[person.userid] = person.userID

		groups_of_five = [[]]
		group_count = 0

		for value_group in sorted(list(values.keys()))[::-1]:
			current_value_group = sorted(values[value_group])
			count = 0

			for person in current_value_group:
				if count == 5:
					group_count += 1
					groups_of_five.append([])
					count = 0

				groups_of_five[group_count].append(person)
				count += 1

		result_key = {}
		result_count = 1

		for group in groups_of_five:

			send_str = ''

			for person in group:
				result_key[result_count] = person.userID
				result_count += 1

				send_str += f'**Name:** {result_number}\n**Description:** {person.helperdescription}\n**Willing to help with:** {person.helptype}\n\n'

			if group != groups_of_five[-1]:
				send_str += '*Please type #show to show more helpers.*'

			send_str += '*Please type #choose and a person\'s result number to start a private room with them.*'

			message = client.wait_for_message(timeout=600, author=ctx.message.author, check=lambda message: message.startswith('#choose ') or message == '#next')

			if message is None:
				break

			elif message == '#next':
				send_overflow(send_str, ctx.message.author)

			elif message.startswith('#choose '):
				helper_num = message.split(' ')[1]
				helper = result_key[int(helper_num)]

				openhelpers.remove(helper)
				openhelp.remove(user)
				break



	@make_connection.add_command(name='helper')
	async def looking_to_help(ctx):
		values = {}
		help_needed = {}

		user = users.load_user(ctx.message.author.id)
		issues = user.issues
		openhelpers.append(user)

		for person in openhelp:
			help_value = 0

			for issue in person.issues:
				if issue in user.helptype:
					help_value += 1

			if help_value not in values.keys():
				values[help_value] = person.userID

			else:
				values[help_value].append(person.userID)

			help_needed[person.userid] = person.userID

		client.send_message('You are now in the helpers queue! Do you wish to manually look for someone to help? Type Yes or Y to confirm. Any other message will be declination. If you do not respond in 10 minutes, you will be removed from the queue.')
		wait_bool = client.wait_for_message(timeout=600, author=ctx.message.author, check=lambda message: message.startswith('#choose ') or message == '#next')

		if wait_bool.upper() == 'Y' or wait_bool.capitalize() == 'Yes':
			groups_of_five = [[]]
			group_count = 0

			for value_group in sorted(list(values.keys()))[::-1]:
				current_value_group = sorted(values[value_group])
				count = 0

				for person in current_value_group:
					if count == 5:
						group_count += 1
						groups_of_five.append([])
						count = 0

					groups_of_five[group_count].append(person)
					count += 1

			result_key = {}
			result_count = 1

			for group in groups_of_five:

				send_str = ''

				for person in group:
					result_key[result_count] = person.userID
					result_count += 1

					send_str += f'**Name:** {result_number}\n**Description:** {person.helpdescription}\n**Needs help with:** {person.issues}\n\n'

				if group != groups_of_five[-1]:
					send_str += '*Please type #show to show more helpers.*'

				send_str += '*Please type #choose and a person\'s result number to start a private room with them.*'

				message = client.wait_for_message(timeout=600, author=ctx.message.author, check=lambda message: message.startswith('#choose ') or message == '#next')

				if message is None:
					break

				elif message == '#next':
					send_overflow(send_str, ctx.message.author)

				elif message.startswith('#choose '):
					help_num = message.split(' ')[1]
					helpee = result_key[int(help_num)]

					openhelp.remove(helpee)
					openhelpers.remove(user)
					break

		elif wait_bool is None:
			openhelpers.remove(user)

def setup(bot):
	bot.add_cog(Connection(bot))