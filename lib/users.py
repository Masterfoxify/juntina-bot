from sys import path
from sqlite3 import connect

path.append('../')

class User:
	def __init__(self, userID, infractions, ruleseverity, mentalseverity, sessionID, helptype, issues, helpdescription, helperdescription):
		self.userID = userID
		self.infractions = infractions
		self.ruleseverity = ruleseverity
		self.mentalseverity = mentalseverity
		self.sessionID = sessionID
		self.helptype = helptype
		self.issues = issues
		self.helpdescription = helpdescription
		self.helperdescription = helperdescription

def load_user(userID):
	connection = connect('userdat.db')
	cursor = connection.cursor()

	cursor.execute('SELECT * FROM userdat WHERE userid=?', userID)

	user_info = [] * 9

	for i in user_info:
		i = cursor.fetchone()

	cursor.close()

	return User(*user_info)